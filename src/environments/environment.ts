// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAkUf9AYpYNSbd7UewAFY_sNI7184-IMNo",
    authDomain: "fulymovies.firebaseapp.com",
    databaseURL: "https://fulymovies.firebaseio.com",
    projectId: "fulymovies",
    storageBucket: "fulymovies.appspot.com",
    messagingSenderId: "890688258069",
    appId: "1:890688258069:web:c90893b4ba9c6f353bce1c",
    measurementId: "G-PD25J399PL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
