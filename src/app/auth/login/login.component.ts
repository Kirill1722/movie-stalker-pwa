import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {User} from "firebase";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public user: User;

  constructor(private angularFireAuth: AngularFireAuth,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.listen()
  }

  public loginWithGoogle(): void {
    this.authService.login();
  }

  public logout(): void {
    this.authService.logout()
  }

  private listen(): void {
    this.angularFireAuth.authState.subscribe(user => {
        this.user = user;
        this.authService.user = user;
      }
    )
  }

}
