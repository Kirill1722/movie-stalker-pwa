import {Component, Input, OnInit} from '@angular/core';
import {Movie} from "../model/movie.model";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit {

  @Input()
  public movie: Movie = {
    title: "Avatar",
    year: 2020,
    image: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
    action: false,
    drama: false,
  }

  constructor() { }

  ngOnInit(): void {
  }

}
