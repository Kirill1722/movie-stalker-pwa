export interface Movie {
  title?: string
  year?: number
  action?: boolean
  drama?: boolean
  rating?: number
  image?: string
}
