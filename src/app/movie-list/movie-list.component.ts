import { Component, OnInit } from '@angular/core';
import {Movie} from "../model/movie.model";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html'
})
export class MovieListComponent implements OnInit {

  public movie1: Movie = {
    title: "Avatar",
    year: 2020,
    image: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
    action: false,
    drama: false,
  }

  public movie2: Movie = {
    title: "Avatar",
    year: 2020,
    image: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
    action: false,
    drama: false,
  }

  public movies: Movie[] = [this.movie1, this.movie2];


  constructor() { }

  ngOnInit(): void {
  }

}
