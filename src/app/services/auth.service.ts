import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {auth, User} from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user: User

  constructor(private angularFireAuth: AngularFireAuth) {
  }

  public login(): void {
    this.angularFireAuth.signInWithRedirect(new auth.GoogleAuthProvider())
      .then(() => console.log('Redirect to Google login'))
  }

  public logout(): void {
    this.angularFireAuth.signOut()
      .then(() => console.log('logout'))
  }

}
